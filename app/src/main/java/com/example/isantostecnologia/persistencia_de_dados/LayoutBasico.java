package com.example.isantostecnologia.persistencia_de_dados;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class LayoutBasico extends AppCompatActivity {

    private TextInputEditText textInputEditText;
    private TextView textView;
    private CheckBox checkBranco, checkVerde, checkVermelho;
    private List<String> check = new ArrayList<String>();
    private RadioGroup rgEstoque;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_basico);

        textView = findViewById(R.id.txtResultado);
        textInputEditText = findViewById(R.id.editText);

        checkBranco = findViewById(R.id.checkBranco);
        checkVerde = findViewById(R.id.checkVerde);
        checkVermelho = findViewById(R.id.checkVermelho);

        rgEstoque = findViewById(R.id.rgEstoque);

        verificaRadioButton();
    }

    public void verificaCheck(){

        check.clear();

        if(checkBranco.isChecked()){
            check.add(checkBranco.getText().toString());
        }
        if(checkVerde.isChecked()){
            check.add(checkVerde.getText().toString());
        }
        if(checkVermelho.isChecked()){
            check.add(checkVermelho.getText().toString());
        }

        String texto = check.toString().replace("[", "").replace("]", "").replace(","," -");
        textView.setText(texto);

    }

    public void verificaRadioButton (){

        rgEstoque.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.radioSim){
                    textView.setText("Sim");
                }else{
                    textView.setText("Não");
                }
            }
        });
    }

    public void enviar(View view) {

        /*String nomeProduto = textInputEditText.getText().toString();
        textView.setText(nomeProduto);*/

        //verificaCheck();

        //Fechar o teclado após o click
        InputMethodManager imm = (InputMethodManager) new Activity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm.isActive())
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }
}

