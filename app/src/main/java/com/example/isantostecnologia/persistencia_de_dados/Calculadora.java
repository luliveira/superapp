package com.example.isantostecnologia.persistencia_de_dados;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class Calculadora extends AppCompatActivity {

    private EditText editValor;
    private TextView txtPorcentagem;
    private TextView txtGorjeta;
    private TextView txtTotal;
    private SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        editValor = findViewById(R.id.editValor);
        txtPorcentagem = findViewById(R.id.txtPorcentagem);
        txtGorjeta = findViewById(R.id.txtGorjeta);
        txtTotal = findViewById(R.id.txtTotal);
        seekBar = findViewById(R.id.seekBar);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                double progresso = 0;
                progresso = seekBar.getProgress();
                txtPorcentagem.setText(Math.round(progresso) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    public void calcula(double progresso) {

        //Recupera o valor digitado
        Double valorDigitado = Double.parseDouble(editValor.getText().toString());

        //calcula a gorjeta e o total
        Double gorjeta = valorDigitado * (progresso / 100);
        Double total = gorjeta + valorDigitado;

        //exibi a gorjeta e o total
        txtGorjeta.setText("R$ " + Math.round(gorjeta));
        txtTotal.setText("R$ " + total);
    }
}
