package com.example.isantostecnologia.persistencia_de_dados;

import android.content.Context;
import android.content.SharedPreferences;

public class AnotacaoPreferencias{

    private Context context;
    private SharedPreferences preferences;
    private final String NOME_ARQUIVO = "anotação.preferencias";
    private SharedPreferences.Editor editor;
    private final String NOME = "nome";

    public AnotacaoPreferencias(Context context){
        this.context = context;
        preferences = context.getSharedPreferences(NOME_ARQUIVO, 0);
        editor = preferences.edit();

    }

    public void salvarAnotacao (String string){
        editor.putString(NOME, string);
        editor.commit();
    }
    public String recuperarAnotacao(){
        return preferences.getString(NOME, " ");
    }
}
