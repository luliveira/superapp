package com.example.isantostecnologia.persistencia_de_dados;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class AlertDialog extends AppCompatActivity {

    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_dialog);

        button = findViewById(R.id.button);
    }

    public void abrirAlerta(View view){

        //Criar alerta
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);

        //Titulo e Mensagem
        dialog.setTitle("SUPER APP");
        dialog.setMessage("Bem vindo!");

        //Definir icone
        dialog.setIcon(android.R.drawable.ic_delete);

        //Configura o cancelamento
        dialog.setCancelable(false);

        //Configura ações do botão
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "Button yes press.", Toast.LENGTH_LONG).show();
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "Button no press.", Toast.LENGTH_LONG).show();
            }
        });

        dialog.create();
        dialog.show();

    }
}
