package com.example.isantostecnologia.persistencia_de_dados;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

public class PBar extends AppCompatActivity {

    private ProgressBar progressBar;
    private ProgressBar progressBarCarregamento;
    private int carregamento = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);

        progressBar = findViewById(R.id.pBarButton);
        progressBarCarregamento = findViewById(R.id.pBar2);

        progressBarCarregamento.setVisibility(View.GONE);
    }

    public void progresso(View view) {
        progressBarCarregamento.setVisibility(View.VISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <= 100; i++) {
                    System.out.println(i);
                    final int PROGRESSO = i;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(PROGRESSO);
                            if (PROGRESSO == 100) {
                                progressBarCarregamento.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "Concluido", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }
}
