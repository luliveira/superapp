package com.example.isantostecnologia.persistencia_de_dados;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GasolinaActivity extends AppCompatActivity {

    private EditText precoAlcool;
    private EditText precoGasolina;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gasolina);

        precoAlcool = findViewById(R.id.precoAlcool);
        precoGasolina = findViewById(R.id.precoGasolina);
        resultado = findViewById(R.id.resultado);
    }

    public void calcularPreco(View view) {

        //recuperar valores
        String gasolina = precoAlcool.getText().toString();
        String alcool = precoGasolina.getText().toString();

        //validar campos
        Boolean validado = isValido(alcool, gasolina);
        if (validado == false) {
            Toast.makeText(getApplicationContext(), "Valores inválido", Toast.LENGTH_SHORT).show();
        } else {
            calculadora(alcool, gasolina);
        }

    }

    public Boolean isValido(String pAlcool, String pGasolina) {
        if (pAlcool == null || pAlcool.equals("")) {
            return false;
        } else if (pGasolina == null || pGasolina.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    public void calculadora(String vAlcool, String vGasolina) {
        //Converter valors String para Double
        double valorAlcool = Double.valueOf(vAlcool);
        double valorGasolina = Double.valueOf(vGasolina);

        //
        Double resultado = valorAlcool / valorGasolina;
        if (resultado >= 0.7) {
            this.resultado.setText("Melhor utilizar Gasolina");
        } else {
            this.resultado.setText("Melhor utilizar Alcool");
        }


    }


}
