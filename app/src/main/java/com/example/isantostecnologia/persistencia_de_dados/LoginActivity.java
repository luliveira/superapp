package com.example.isantostecnologia.persistencia_de_dados;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private TextInputEditText login;
    private TextInputEditText senha;
    private Button logar;
    private final String ARQUIVO = "acesso";
    private final String LOGIN = "login";
    private final String SENHA = "senha";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login = findViewById(R.id.txtLogin);
        senha = findViewById(R.id.txtSenha);
        logar = findViewById(R.id.btnLogin);
        sharedPreferences = getSharedPreferences(ARQUIVO, 0);
        editor = sharedPreferences.edit();

        if (sharedPreferences.contains(LOGIN) && sharedPreferences.contains(SENHA)){
            startActivity(new Intent(LoginActivity.this, PricipalActivity.class));
            finish();
        }
    }

    public void logar(View view) {
        Boolean resultadoLogin = verificaLogin(this.login);
        Boolean resultadoSenha = verificaSenha(this.senha);
        if (!resultadoLogin || !resultadoSenha) {
            Toast.makeText(getApplicationContext(), "Login e Senha inválidos", Toast.LENGTH_LONG).show();
        } else {
            startActivity(new Intent(LoginActivity.this, PricipalActivity.class));
            finish();
        }
    }

    public Boolean verificaLogin(TextInputEditText login) {
        if (login.getText().toString() == null || login.getText().toString().equals("")) {
            return false;
        }
        this.editor.putString(LOGIN, login.getText().toString());
        this.editor.commit();
        return true;
    }

    public Boolean verificaSenha(TextInputEditText senha) {

        if (senha.getText().toString() == null || senha.getText().toString().equals("")) {
            return false;
        }
        this.editor.putString(SENHA, senha.getText().toString());
        this.editor.commit();
        return true;
    }


}
