package com.example.isantostecnologia.persistencia_de_dados;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class PricipalActivity extends AppCompatActivity {

    private Button anotacoes;
    private Button gravarNome;
    private Button chain;
    private Button basicLayout;
    private Button gasolina;
    private Button gasolina1;
    private Button dialog;
    private Button pBar;
    private Button calculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal);

        pBar = findViewById(R.id.pBarButton);
        pBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PricipalActivity.this, PBar.class));
            }
        });

        //APP ANOTAÇÕES
        anotacoes = findViewById(R.id.btnAnotacoes);
        anotacoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PricipalActivity.this, Main2Activity.class));
            }
        });

        //APP GRAVAR NOME
        gravarNome = findViewById(R.id.btnGravarNome);
        gravarNome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PricipalActivity.this, MainActivity.class));
            }
        });

        //APP DE LAYOUT E COMPONENTS
        basicLayout = findViewById(R.id.btnBasicLayout);
        basicLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PricipalActivity.this, LayoutBasico.class));
            }
        });

        gasolina = findViewById(R.id.gasolina);
        gasolina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(PricipalActivity.this, GasolinaActivity.class));
            }
        });

        dialog = findViewById(R.id.dialog);
        dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PricipalActivity.this, AlertDialog.class));
            }
        });

        calculadora = findViewById(R.id.calculadora);
        calculadora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PricipalActivity.this, Calculadora.class));
            }
        });

    }

    public void escolha(View view) {
        Toast.makeText(getApplicationContext(), "Escolha uma opção", Toast.LENGTH_LONG).show();
    }
}





