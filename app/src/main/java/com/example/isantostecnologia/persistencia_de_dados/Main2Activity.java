package com.example.isantostecnologia.persistencia_de_dados;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    private FloatingActionButton voltar;
    private EditText editText;
    private FloatingActionButton button;
    private static final String ANOTACOES = "Anotações";
    private AnotacaoPreferencias anotacaoPreferencias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        button = findViewById(R.id.fab);
        editText = findViewById(R.id.editText2);

        //CLICK DO BOTÃO VOLTAR PARA VOLTAR PARA TELA PRINCIPAL
        voltar = findViewById(R.id.btnVoltar);
        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Main2Activity.this, PricipalActivity.class));

                anotacaoPreferencias = new AnotacaoPreferencias(getApplicationContext());
            }
        });


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validar se foi digitado algo
                if (editText.getText().toString().equals("")) {
                    Snackbar.make(view, "Preencha a anotação", Snackbar.LENGTH_LONG).show();
                } else {
                    anotacaoPreferencias.salvarAnotacao(editText.getText().toString());
                    Snackbar.make(view, "Anotação salva com sucesso", Toast.LENGTH_LONG).show();
                }

            }
        });


        String anotacao = anotacaoPreferencias.recuperarAnotacao();
        if (!anotacao.equals("")) {
            editText.setText(anotacao);
        }

    }

}