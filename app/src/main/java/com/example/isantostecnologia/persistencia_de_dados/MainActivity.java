package com.example.isantostecnologia.persistencia_de_dados;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button botaoSalvar;
    private TextInputEditText editTextNome;
    private TextView editTextResultado;
    private static final String ARQUIVO_PREFERENCIA = "Arquivo_Preferencia";
    private Button voltar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botaoSalvar = findViewById(R.id.botaoSalvar);
        editTextNome = findViewById(R.id.editText);
        editTextResultado = findViewById(R.id.textView2);

        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mode 0 = modo privado (apenas o aplicativo consegue salvar e ler o arquivo
                SharedPreferences preferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                SharedPreferences.Editor editor = preferences.edit();

                //Validar o nome
                if (editTextNome.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Preencha o nome", Toast.LENGTH_LONG).show();
                } else {
                    String nome = editTextNome.getText().toString();
                    editor.putString("nome", nome);
                    editor.commit();
                    editTextResultado.setText("Bem vindo, " + nome);


                }
            }
        });
        //Recuperar os arquivos salvo
        SharedPreferences preferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        //Valida se temos o nome dentro de prefereces
        if (preferences.contains("nome")) {

            String nome = preferences.getString("nome", "não definido");
            editTextResultado.setText("Bem vindo, " + nome);

        } else {
            editTextResultado.setText("Bem vindo, usuário não definido");
        }

        voltar = findViewById(R.id.button2);
        voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, PricipalActivity.class));
            }
        });

    }
}
